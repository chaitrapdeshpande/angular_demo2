(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-router.module.ts":
/*!**************************************!*\
  !*** ./src/app/app-router.module.ts ***!
  \**************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var routes = [
    { path: '', component: _login_login_component__WEBPACK_IMPORTED_MODULE_2__["LoginComponent"] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _utils_index__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./utils/index */ "./src/app/utils/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent(s3util, storageUtil) {
        this.s3util = s3util;
        this.storageUtil = storageUtil;
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        }),
        __metadata("design:paramtypes", [_utils_index__WEBPACK_IMPORTED_MODULE_1__["S3Util"], _utils_index__WEBPACK_IMPORTED_MODULE_1__["StorageUtil"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: tokenGetter, AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "tokenGetter", function() { return tokenGetter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @auth0/angular-jwt */ "./node_modules/@auth0/angular-jwt/index.js");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _app_router_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./app-router.module */ "./src/app/app-router.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _material_material_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./material/material.module */ "./src/app/material/material.module.ts");
/* harmony import */ var _angular_flex_layout__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/flex-layout */ "./node_modules/@angular/flex-layout/esm5/flex-layout.es5.js");
/* harmony import */ var _error_handler_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./error-handler.service */ "./src/app/error-handler.service.ts");
/* harmony import */ var _services_index__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./services/index */ "./src/app/services/index.ts");
/* harmony import */ var _helpers_index__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./helpers/index */ "./src/app/helpers/index.ts");
/* harmony import */ var _utils_index__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./utils/index */ "./src/app/utils/index.ts");
/* harmony import */ var _simple_flex_simple_flex_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./simple-flex/simple-flex.component */ "./src/app/simple-flex/simple-flex.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

















function tokenGetter() {
    return localStorage.getItem('token');
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"],
                _simple_flex_simple_flex_component__WEBPACK_IMPORTED_MODULE_15__["SimpleFlexComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_8__["BrowserAnimationsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
                _material_material_module__WEBPACK_IMPORTED_MODULE_9__["MaterialModule"],
                _angular_flex_layout__WEBPACK_IMPORTED_MODULE_10__["FlexLayoutModule"],
                _app_router_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"],
                _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_4__["JwtModule"].forRoot({
                    config: {
                        tokenGetter: tokenGetter,
                        whitelistedDomains: []
                    }
                }),
            ],
            providers: [
                _error_handler_service__WEBPACK_IMPORTED_MODULE_11__["HttpErrorHandler"],
                { provide: _angular_core__WEBPACK_IMPORTED_MODULE_1__["ErrorHandler"], useClass: _error_handler_service__WEBPACK_IMPORTED_MODULE_11__["HttpErrorHandler"] },
                { provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HTTP_INTERCEPTORS"], useClass: _helpers_index__WEBPACK_IMPORTED_MODULE_13__["InterceptorService"], multi: true },
                _services_index__WEBPACK_IMPORTED_MODULE_12__["LoginService"],
                _utils_index__WEBPACK_IMPORTED_MODULE_14__["S3Util"],
                _utils_index__WEBPACK_IMPORTED_MODULE_14__["CognitoUtil"],
                _utils_index__WEBPACK_IMPORTED_MODULE_14__["StorageUtil"]
            ],
            bootstrap: [
                _app_component__WEBPACK_IMPORTED_MODULE_7__["AppComponent"]
            ]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/error-handler.service.ts":
/*!******************************************!*\
  !*** ./src/app/error-handler.service.ts ***!
  \******************************************/
/*! exports provided: HttpErrorHandler */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttpErrorHandler", function() { return HttpErrorHandler; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var HttpErrorHandler = /** @class */ (function () {
    function HttpErrorHandler() {
    }
    /*
    * Returns a function that handles Http operation failures.
    * This error handler lets the app continue to run as if no error occurred.
    */
    HttpErrorHandler.prototype.handleError = function (error) {
        if (error instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpErrorResponse"]) {
            console.log('ERROR', error);
        }
        else {
            // Handle Client Error (Angular Error, ReferenceError...)
        }
        console.log('Error', error);
    };
    HttpErrorHandler = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], HttpErrorHandler);
    return HttpErrorHandler;
}());



/***/ }),

/***/ "./src/app/helpers/auth.service.ts":
/*!*****************************************!*\
  !*** ./src/app/helpers/auth.service.ts ***!
  \*****************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @auth0/angular-jwt */ "./node_modules/@auth0/angular-jwt/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


// import { AuthGuard, AdminGuard } from '../helpers/auth.guard';
var AuthService = /** @class */ (function () {
    function AuthService(jwtHelper) {
        this.jwtHelper = jwtHelper;
    }
    AuthService.prototype.getAuthorizationToken = function () {
        return this.jwtHelper.tokenGetter();
    };
    AuthService.prototype.isAuthenticated = function () {
        var token = this.jwtHelper.tokenGetter();
        if (!token) {
            return false;
        }
        var tokenExpired = this.jwtHelper.isTokenExpired(token);
        return !tokenExpired;
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_auth0_angular_jwt__WEBPACK_IMPORTED_MODULE_1__["JwtHelperService"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/helpers/index.ts":
/*!**********************************!*\
  !*** ./src/app/helpers/index.ts ***!
  \**********************************/
/*! exports provided: InterceptorService, AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _interceptor_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./interceptor.service */ "./src/app/helpers/interceptor.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "InterceptorService", function() { return _interceptor_service__WEBPACK_IMPORTED_MODULE_0__["InterceptorService"]; });

/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth.service */ "./src/app/helpers/auth.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return _auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]; });





/***/ }),

/***/ "./src/app/helpers/interceptor.service.ts":
/*!************************************************!*\
  !*** ./src/app/helpers/interceptor.service.ts ***!
  \************************************************/
/*! exports provided: InterceptorService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InterceptorService", function() { return InterceptorService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./auth.service */ "./src/app/helpers/auth.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var InterceptorService = /** @class */ (function () {
    function InterceptorService(auth) {
        this.auth = auth;
    }
    InterceptorService.prototype.intercept = function (request, next) {
        // Get the auth token from the service.
        var authToken = this.auth.getAuthorizationToken();
        /*
        * The verbose way:
        // Clone the request and replace the original headers with
        // cloned headers, updated with the authorization.
        const authReq = req.clone({
          headers: req.headers.set('Authorization', authToken)
        });
        */
        // Clone the request and set the new header in one step.
        // const authReq = req.clone({ setHeaders: { Authorization: authToken } });
        // send cloned request with header to the next handler.
        // return next.handle(authReq);
        return next.handle(request);
    };
    InterceptorService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"]])
    ], InterceptorService);
    return InterceptorService;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".bounds {\n    background-color:#ddd;\n    height :100px;\n  }\n  \n.content {\n    min-width: 100px;\n    /*height: 400px;*/\n  }\n  \n.sec1 {\n    background: purple;\n    color:white;\n    \n  }\n  \n.sec2 {\n    background: yellow;\n    color:blue;\n    \n  }\n  \n.sec3 {\n    background: blue;\n    color:white;\n    \n  }\n  \n.sec4 {\n    background: green;\n    color:white;\n    \n  }\n  \n.sec1, .sec2, .sec3, .sec4 {\n    padding-top:20px;\n    text-align:center\n  }\n  \n.flex-container{\n\n  background: blue;\n    color:white;\n}\n  \n.flex-item{\n    background: purple;\n    color:white;\n}"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<!--<mat-checkbox>Check</mat-checkbox><br>\n<mat-radio-group>Radio</mat-radio-group><br>\n<mat-slider>slider</mat-slider><br>\n<mat-slide-toggle>slide toggle</mat-slide-toggle><br>\n<mat-menu>menu</mat-menu><br>\n<mat-card>Card</mat-card>\n<mat-divider>Mat divider</mat-divider>-->\n\n\n<!--<div class=\"lable-container\" fxLayout fxLayout.xs=\"column\" fxLayoutAlign=\"center center\" fxLayoutGap=\"10px\" fxLayoutGap.xs=\"0\">\n    <div fxFlex=\"30%\">\n        <form name=\"form\" (ngSubmit)=\"f.form.valid && onSubmit()\" #f=\"ngForm\">\n             [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\"\n            <div class=\"content\" fxLayout=\"row\" fxLayoutGap=\"0px\" fxLayoutAlign=\"center center\">\n            <mat-card style=\"width: 100%; padding: 0;\">\n                <div fxFlex=100>\n                    <div fxLayout=\"row\" fxLayoutGap=\"0px\" style=\"margin-top: 10px;\">\n                        <div fxFlex=100 style=\"text-align: center;margin-top: 20px;\">\n                            <h1 class=\"mat-headline\">LOGIN</h1>\n                        </div>\n                    </div>\n                    <div fxLayout=\"row\" fxLayoutGap=\"0px\" fxLayoutAlign=\"center\">\n                        <div fxFlex=10>\n                        </div>\n                        <div fxFlex=80>\n                            <mat-form-field class=\"full-width\" appearance=\"outline\">\n                                <mat-label>Email</mat-label>\n                                <input matInput autofocus placeholder=\"User Name\" name=\"userName\" type=\"email\"  [(ngModel)]=\"loginModel.userName\"\n                                #userName=\"ngModel\" [ngClass]=\"{ 'is-invalid': f.submitted && userName.invalid }\" required>\n                            </mat-form-field>\n                        </div>\n                        <div fxFlex=10>\n                        </div>\n                    </div>\n                    <div fxLayout=\"row\" fxLayoutGap=\"0px\" fxLayoutAlign=\"center\">\n                        <div fxFlex=10>\n                        </div>\n                        <div fxFlex=80>\n                            <mat-form-field class=\"full-width\" appearance=\"outline\">\n                                <mat-label>Password</mat-label>\n                                <input matInput placeholder=\"Password\" type=\"password\" name=\"password\"  [(ngModel)]=\"loginModel.password\"\n                                #password=\"ngModel\" [ngClass]=\"{ 'is-invalid': f.submitted && password.invalid }\" required>\n                                <mat-hint *ngIf=\"submitted &&loginError\" style=\"color:red\">{{errorMessage}}</mat-hint>\n                            </mat-form-field>\n                        </div>\n                        <div fxFlex=10>\n                        </div>\n                    </div>\n                    <div fxLayout=\"row\" fxLayoutGap=\"10px\" fxLayoutAlign=\"center\">\n                        <div fxFlex=100>\n                        <button style=\"height: 50px;margin-top: 4px;\" mat-raised-button color=\"primary\" class=\"full-width mat-headline\">Login</button>\n                        </div>\n                    </div>\n                </div>\n            </mat-card>\n            </div>\n        </form>\n    </div>\n</div> -->\n\n<mat-card class = \"sec1\">This is Card</mat-card><br><br>\n\n<div class=\"bounds\">\n    \n    <div class=\"content\" \n         fxLayout=\"row\" \n         fxLayout.xs=\"column\" \n         fxFlexFill >\n         \n        <div fxFlex=\"15\" class=\"sec1\" fxFlex.xs=\"55\">\n            ONE\n        </div>\n        <div fxFlex=\"30\" class=\"sec2\" >\n            TWO\n        </div>\n        <div fxFlex=\"55\" class=\"sec3\" fxFlex.xs=\"15\">\n            THREE\n        </div>\n        \n    </div>\n    \n  </div><br><br>\n  \n<div class = \"bounds\">\n    <div class = \"content\">\n        <div fxLayout = \"column\" >\n            <div fxFlex = \"15\" class=\"sec1\">One</div>\n            <div fxFlex = \"15\" class=\"sec2\">Two</div>\n            <div fxFlex = \"15\" class=\"sec3\">Three</div>\n            <div fxFlex = \"15\" class=\"sec4\">Four</div>\n        </div>\n    </div>\n</div><br><br><br><br>\n\n\n<!--\n<h2>Mobile Layout</h2>\n\n<div fxLayout=\"column\">\n    <header>header</header>\n    <div fxLayout=\"row\" fxLayout.xs = \"column\" fxFlex>\n        <nav fxFlex=\"1 6 20%\" fxFlexOrder\n             fxFlexOrder.xs = \"2\">nav</nav>\n\n        <article fxFlex=\"3 1 60%\" fxFlexOrder\n             fxFlexOrder.xs = \"1\">article</article>\n\n        <aside fxFlex=\"1 6 20%\" fxFlexOrder\n             fxFlexOrder.xs = \"3\">aside</aside>\n    </div>\n    <footer>footer</footer>\n</div>-->\n\n\n<mat-toolbar>\n    <mat-toolbar-row>\n      <a target=\"_blank\"  href=\"https://angular.io/\">Angular Doc</a>\n    </mat-toolbar-row>\n    <mat-toolbar-row>\n      <a target=\"_blank\" href=\"https://www.npmjs.com/package/@angular/flex-layout\">Flex-Layout</a>\n    </mat-toolbar-row>\n    <mat-toolbar-row>\n      <a target=\"_blank\" href=\"https://material.angular.io/\">Angular Material Design</a>\n    </mat-toolbar-row>\n</mat-toolbar> <br><br>\n\n\n<!-- Desktop Layout-->\n<div class=\"flex-container\" fxLayout=\"row\" fxLayoutAlign=\"center center\">\n        <div class=\"flex-item\"></div>\n        <div class=\"flex-item\"></div>\n        <div class=\"flex-item\"></div>\n</div>\n\n\n<!--Mobile Layout-->\n<div class=\"flex-container\"\n     fxLayout=\"row\"\n     fxLayout.xs=\"column\"\n     fxLayoutAlign=\"center center\"\n     fxLayoutAlign.xs=\"start\">\n    <div fxFlex = \"15\" class=\"flex-item\"></div>\n    <div class=\"flex-item\"></div>\n    <div class=\"flex-item\"></div>\n</div>\n\n  \n\n\n\n\n"

/***/ }),

/***/ "./src/app/login/login.component.scss":
/*!********************************************!*\
  !*** ./src/app/login/login.component.scss ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-width {\n  width: 100%; }\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_index__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/index */ "./src/app/services/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var LoginComponent = /** @class */ (function () {
    function LoginComponent(formBuilder, loginService) {
        this.formBuilder = formBuilder;
        this.loginService = loginService;
        this.loginModel = {};
    }
    LoginComponent.prototype.ngOnInit = function () { };
    LoginComponent.prototype.onSubmit = function () {
        console.log('Login Model', this.loginModel);
        this.loginService.authenticateUser(this.loginModel.userName, this.loginModel.password);
    };
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.scss */ "./src/app/login/login.component.scss"), __webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormBuilder"], _services_index__WEBPACK_IMPORTED_MODULE_2__["LoginService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/material/material.module.ts":
/*!*********************************************!*\
  !*** ./src/app/material/material.module.ts ***!
  \*********************************************/
/*! exports provided: MaterialModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MaterialModule", function() { return MaterialModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/form-field */ "./node_modules/@angular/material/esm5/form-field.es5.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


//import { MatInputModule, MatButtonModule } from '@angular/material';



var MaterialModule = /** @class */ (function () {
    function MaterialModule() {
    }
    MaterialModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_1__["CommonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBadgeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBottomSheetModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDividerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTreeModule"]
            ],
            exports: [
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_3__["BrowserAnimationsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatToolbarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"],
                _angular_material_form_field__WEBPACK_IMPORTED_MODULE_4__["MatFormFieldModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatGridListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatAutocompleteModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBadgeModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatBottomSheetModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatChipsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatStepperModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDialogModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDividerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatExpansionModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatListModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatMenuModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatPaginatorModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatProgressSpinnerModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRadioModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRippleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSidenavModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSliderModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSlideToggleModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSortModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTabsModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTooltipModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatTreeModule"]
            ],
            declarations: []
        })
    ], MaterialModule);
    return MaterialModule;
}());



/***/ }),

/***/ "./src/app/services/index.ts":
/*!***********************************!*\
  !*** ./src/app/services/index.ts ***!
  \***********************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _login_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.service */ "./src/app/services/login.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return _login_service__WEBPACK_IMPORTED_MODULE_0__["LoginService"]; });




/***/ }),

/***/ "./src/app/services/login.service.ts":
/*!*******************************************!*\
  !*** ./src/app/services/login.service.ts ***!
  \*******************************************/
/*! exports provided: LoginService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginService", function() { return LoginService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _error_handler_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../error-handler.service */ "./src/app/error-handler.service.ts");
/* harmony import */ var _utils_index__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../utils/index */ "./src/app/utils/index.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LoginService = /** @class */ (function () {
    function LoginService(http, cognitoUtil, storageUtil, httpErrorHandler) {
        this.http = http;
        this.cognitoUtil = cognitoUtil;
        this.storageUtil = storageUtil;
        this.httpErrorHandler = httpErrorHandler;
    }
    LoginService.prototype.authenticateUser = function (userName, password) {
        var _this = this;
        var authenticationData = {
            Username: userName,
            Password: password
        };
        this.cognitoUtil.authenticateUser(authenticationData).then(function (token) {
            _this.storageUtil.set('token', token);
        }).catch(function (error) {
            _this.httpErrorHandler.handleError(error);
        });
    };
    LoginService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"],
            _utils_index__WEBPACK_IMPORTED_MODULE_3__["CognitoUtil"],
            _utils_index__WEBPACK_IMPORTED_MODULE_3__["StorageUtil"],
            _error_handler_service__WEBPACK_IMPORTED_MODULE_2__["HttpErrorHandler"]])
    ], LoginService);
    return LoginService;
}());



/***/ }),

/***/ "./src/app/simple-flex/simple-flex.component.css":
/*!*******************************************************!*\
  !*** ./src/app/simple-flex/simple-flex.component.css ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/simple-flex/simple-flex.component.html":
/*!********************************************************!*\
  !*** ./src/app/simple-flex/simple-flex.component.html ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<p>\n  simple-flex works!\n</p>\n\n\n"

/***/ }),

/***/ "./src/app/simple-flex/simple-flex.component.ts":
/*!******************************************************!*\
  !*** ./src/app/simple-flex/simple-flex.component.ts ***!
  \******************************************************/
/*! exports provided: SimpleFlexComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SimpleFlexComponent", function() { return SimpleFlexComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var SimpleFlexComponent = /** @class */ (function () {
    function SimpleFlexComponent() {
    }
    SimpleFlexComponent.prototype.ngOnInit = function () {
    };
    SimpleFlexComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-simple-flex',
            template: __webpack_require__(/*! ./simple-flex.component.html */ "./src/app/simple-flex/simple-flex.component.html"),
            styles: [__webpack_require__(/*! ./simple-flex.component.css */ "./src/app/simple-flex/simple-flex.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], SimpleFlexComponent);
    return SimpleFlexComponent;
}());



/***/ }),

/***/ "./src/app/utils/cognito.service.ts":
/*!******************************************!*\
  !*** ./src/app/utils/cognito.service.ts ***!
  \******************************************/
/*! exports provided: CognitoUtil */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CognitoUtil", function() { return CognitoUtil; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var aws_sdk__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! aws-sdk */ "./node_modules/aws-sdk/lib/browser.js");
/* harmony import */ var aws_sdk__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(aws_sdk__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! amazon-cognito-identity-js */ "./node_modules/amazon-cognito-identity-js/es/index.js");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var CognitoUtil = /** @class */ (function () {
    // public cognitoService : CognitoService;
    function CognitoUtil() {
        this.setAWSCreadentials();
        CognitoUtil_1.userPool = this.getUserPool();
        CognitoUtil_1.cognitoIdentityServiceProvider = this.getAWSServiceProvider();
    }
    CognitoUtil_1 = CognitoUtil;
    CognitoUtil.prototype.getUserPool = function () {
        return new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_2__["CognitoUserPool"](CognitoUtil_1.POOL_DATA);
    };
    CognitoUtil.prototype.getAWSServiceProvider = function () {
        return new aws_sdk__WEBPACK_IMPORTED_MODULE_1__["CognitoIdentityServiceProvider"]();
    };
    CognitoUtil.prototype.createAuthenticationCreadentials = function (authData) {
        return new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_2__["AuthenticationDetails"](authData);
    };
    CognitoUtil.prototype.getCognitoUser = function (userName) {
        var userData = {
            Username: userName,
            Pool: CognitoUtil_1.userPool
        };
        return new amazon_cognito_identity_js__WEBPACK_IMPORTED_MODULE_2__["CognitoUser"](userData);
    };
    CognitoUtil.prototype.setUserPoolCredentials = function (userPool) {
        // let userPool: CognitoUserPool = this.getUserPool();
        userPool['client'].config.region = CognitoUtil_1.REGION;
        userPool['client'].config.update({
            accessKeyId: CognitoUtil_1.ACCESS_KEY,
            secretAccessKey: CognitoUtil_1.SECRET_ACCESS_KEY
        });
    };
    CognitoUtil.prototype.setAWSCreadentials = function () {
        aws_sdk__WEBPACK_IMPORTED_MODULE_1__["config"].update({
            region: CognitoUtil_1.REGION,
            accessKeyId: CognitoUtil_1.ACCESS_KEY,
            secretAccessKey: CognitoUtil_1.SECRET_ACCESS_KEY
        });
    };
    CognitoUtil.prototype.getCurrentUser = function () {
        return CognitoUtil_1.userPool.getCurrentUser();
    };
    CognitoUtil.prototype.authenticateUser = function (authCreadential) {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            var authenticationDetails = _this.createAuthenticationCreadentials(authCreadential);
            var cognitoUser = _this.getCognitoUser(authCreadential.Username);
            cognitoUser.authenticateUser(authenticationDetails, {
                onSuccess: function (result) {
                    var token = result.getAccessToken().getJwtToken();
                    resolve(token);
                },
                onFailure: function (error) {
                    localStorage.clear();
                    reject(error);
                },
                newPasswordRequired: function (userAttributes, requiredAttributes) {
                    var newPassword = authCreadential.password;
                    var attributesData = {};
                    cognitoUser.completeNewPasswordChallenge(newPassword, attributesData, this);
                }
            });
        });
        return promise;
    };
    CognitoUtil.prototype.getUserList = function () {
        var _this = this;
        var promise = new Promise(function (resolve, reject) {
            _this.setUserPoolCredentials(CognitoUtil_1.userPool);
            CognitoUtil_1.userPool['client'].listUsers({
                'UserPoolId': CognitoUtil_1.USER_POOL_ID
            }, function (data, list) {
                if (list) {
                    resolve(list);
                }
                else {
                    reject();
                }
            });
        });
        return promise;
    };
    CognitoUtil.prototype.adminCreateUser = function (params) {
        return new Promise(function (resolve, reject) {
            CognitoUtil_1.cognitoIdentityServiceProvider.adminCreateUser(params, function (error, data) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(data);
                }
            });
        });
    };
    CognitoUtil.prototype.adminDeleteUser = function (params) {
        return new Promise(function (resolve, reject) {
            CognitoUtil_1.cognitoIdentityServiceProvider.adminDeleteUser(params, function (error, data) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(data);
                }
            });
        });
    };
    CognitoUtil.prototype.adminAddUserToGroup = function (params) {
        return new Promise(function (resolve, reject) {
            CognitoUtil_1.cognitoIdentityServiceProvider.adminAddUserToGroup(params, function (error, data) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(data);
                }
            });
        });
    };
    CognitoUtil.prototype.adminResetUserPassword = function (params) {
        return new Promise(function (resolve, reject) {
            CognitoUtil_1.cognitoIdentityServiceProvider.adminResetUserPassword(params, function (error, data) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(data);
                }
            });
        });
    };
    CognitoUtil.prototype.listGroup = function (params) {
        return new Promise(function (resolve, reject) {
            CognitoUtil_1.cognitoIdentityServiceProvider.listGroups(params, function (error, data) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(data);
                }
            });
        });
    };
    CognitoUtil.prototype.adminInitiateAuth = function (params) {
        return new Promise(function (resolve, reject) {
            CognitoUtil_1.cognitoIdentityServiceProvider.adminInitiateAuth(params, function (error, data) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(data);
                }
            });
        });
    };
    CognitoUtil.REGION = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].REGION;
    CognitoUtil.ACCESS_KEY = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].ACCESS_KEY;
    CognitoUtil.SECRET_ACCESS_KEY = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].SECRET_ACCESS_KEY;
    CognitoUtil.IDENTITY_POOL_ID = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].IDENTITY_POOL_ID;
    CognitoUtil.USER_POOL_ID = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].USER_POOL_ID;
    CognitoUtil.CLIENT_ID = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].CLIENT_ID;
    CognitoUtil.POOL_DATA = {
        UserPoolId: CognitoUtil_1.USER_POOL_ID,
        ClientId: CognitoUtil_1.CLIENT_ID
    };
    CognitoUtil = CognitoUtil_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], CognitoUtil);
    return CognitoUtil;
    var CognitoUtil_1;
}());



/***/ }),

/***/ "./src/app/utils/index.ts":
/*!********************************!*\
  !*** ./src/app/utils/index.ts ***!
  \********************************/
/*! exports provided: CognitoUtil, S3Util, StorageUtil */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _cognito_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./cognito.service */ "./src/app/utils/cognito.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "CognitoUtil", function() { return _cognito_service__WEBPACK_IMPORTED_MODULE_0__["CognitoUtil"]; });

/* harmony import */ var _s3_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./s3.service */ "./src/app/utils/s3.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "S3Util", function() { return _s3_service__WEBPACK_IMPORTED_MODULE_1__["S3Util"]; });

/* harmony import */ var _storage_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./storage.service */ "./src/app/utils/storage.service.ts");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "StorageUtil", function() { return _storage_service__WEBPACK_IMPORTED_MODULE_2__["StorageUtil"]; });






/***/ }),

/***/ "./src/app/utils/s3.service.ts":
/*!*************************************!*\
  !*** ./src/app/utils/s3.service.ts ***!
  \*************************************/
/*! exports provided: S3Util */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "S3Util", function() { return S3Util; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var aws_sdk_clients_s3__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! aws-sdk/clients/s3 */ "./node_modules/aws-sdk/clients/s3.js");
/* harmony import */ var aws_sdk_clients_s3__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(aws_sdk_clients_s3__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../environments/environment */ "./src/environments/environment.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var S3Util = /** @class */ (function () {
    function S3Util() {
        this.init();
    }
    S3Util_1 = S3Util;
    S3Util.prototype.init = function () {
        S3Util_1.s3Instance = new aws_sdk_clients_s3__WEBPACK_IMPORTED_MODULE_1__({
            accessKeyId: S3Util_1.ACCESS_KEY,
            secretAccessKey: S3Util_1.SECRET_ACCESS_KEY,
            region: S3Util_1.REGION
        });
    };
    S3Util.prototype.upload = function (params) {
        return new Promise(function (resolve, reject) {
            S3Util_1.s3Instance.upload(params, function (error, data) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(data);
                }
            });
        });
    };
    S3Util.prototype.putObject = function (params) {
        return new Promise(function (resolve, reject) {
            S3Util_1.s3Instance.putObject(params, function (error, data) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(data);
                }
            });
        });
    };
    S3Util.prototype.getObject = function (params) {
        return new Promise(function (resolve, reject) {
            S3Util_1.s3Instance.getObject(params, function (error, data) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve(data);
                }
            });
        });
    };
    S3Util.REGION = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].REGION;
    S3Util.ACCESS_KEY = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].ACCESS_KEY;
    S3Util.SECRET_ACCESS_KEY = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].SECRET_ACCESS_KEY;
    S3Util = S3Util_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], S3Util);
    return S3Util;
    var S3Util_1;
}());



/***/ }),

/***/ "./src/app/utils/storage.service.ts":
/*!******************************************!*\
  !*** ./src/app/utils/storage.service.ts ***!
  \******************************************/
/*! exports provided: StorageUtil */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorageUtil", function() { return StorageUtil; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var StorageUtil = /** @class */ (function () {
    function StorageUtil() {
    }
    StorageUtil.prototype.set = function (key, value) {
        value = JSON.stringify(value);
        localStorage.setItem(key, value);
    };
    StorageUtil.prototype.get = function (key) {
        return JSON.parse(localStorage.getItem(key));
    };
    StorageUtil.prototype.clearStorage = function () {
        localStorage.clear();
    };
    StorageUtil = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])()
    ], StorageUtil);
    return StorageUtil;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
var environment = {
    production: false,
    REGION: 'us-east-1',
    ACCESS_KEY: 'AKIAJSJMTIMBMF4W74AA',
    SECRET_ACCESS_KEY: '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',
    IDENTITY_POOL_ID: 'us-east-1:fbe0340f-9ffc-4449-a935-bb6a6661fd53',
    USER_POOL_ID: 'us-east-1_P4xn6XyaY',
    CLIENT_ID: '6rf9qi31b95nphok39dqdu91pu'
};


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! hammerjs */ "./node_modules/hammerjs/hammer.js");
/* harmony import */ var hammerjs__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(hammerjs__WEBPACK_IMPORTED_MODULE_4__);





if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"]);


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/prahaladd/Documents/Angular/NuminoAssignments/simple-flex-layout/src/main.ts */"./src/main.ts");


/***/ }),

/***/ 1:
/*!********************!*\
  !*** fs (ignored) ***!
  \********************/
/*! no static exports found */
/***/ (function(module, exports) {

/* (ignored) */

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map